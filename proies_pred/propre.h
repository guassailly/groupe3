#include<stdio.h>
#include<stdlib.h>
#include <time.h>

#define NB_PREDATEURS 3
#define NB_PROIES 2
#define LARGEUR 24
#define HAUTEUR 16

struct predateur{
    int nom;
    int x;    // coordonnée sur x (horizontale)
    int y;    // coordonnée sur y (verticale)
    int vie;  // vie = 0 --> mort ; vie = 1 --> vivant
    //...
    int nom_ami;  // numéro de l'ami prédateur le plus proche
    int nom_proie;
    int dist_proie; // proie proche: 0; loin: 1
};

typedef struct predateur predateur_t;

struct proie{
    int nom;
    int x;
    int y;
    int vie;

    int nom_pred;  //nom du predateur le plus proche.
    int dist_pred; // proche: 0; loin: 1          ---LIMITE A 150px?---
};

typedef struct proie proie_t;

predateur_t * init_pred();

proie_t * init_proie();

void afficher_pred(predateur_t pred);

void afficher_proie(proie_t proie);
