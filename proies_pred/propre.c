#include<stdio.h>
#include<stdlib.h>
#include "propre.h"

predateur_t * init_pred()  //nombre de predateurs
{
    predateur_t * liste_pred=malloc(NB_PREDATEURS*sizeof(predateur_t));
    for (int i=0;i<NB_PREDATEURS;++i)
    {
        liste_pred[i].nom = i;
        liste_pred[i].x = (rand()% 24)*50;  // pour un  terrain de 1200*800px en se deplaçant tous les 50px
        liste_pred[i].y = (rand()% 16)*50;
        liste_pred[i].vie = 1;              // tous les animaux sont vivants au départ.
    }
    return liste_pred;
}

proie_t * init_proie()  //nombre de predateurs
{
    
    proie_t * liste_proie=malloc(NB_PROIES*sizeof(proie_t));
    for (int i=0;i<NB_PROIES;++i)
    {
        liste_proie[i].nom = i;
        liste_proie[i].x = (rand()% LARGEUR)*50;  // pour un  terrain de 1200*800px en se deplaçant tous les 50px
        liste_proie[i].y = (rand()% HAUTEUR)*50;
        liste_proie[i].vie = 1;              // tous les animaux sont vivants au départ.
    }
    return liste_proie;
}

void afficher_pred(predateur_t pred)
{
    printf("nom: %d\n", pred.nom);
    printf("coordonnées: (%d , %d)\n", pred.x, pred.y);
    if (pred.vie == 1)
    {
        printf("vivant\n\n");
    }
    else printf("mort\n\n");
}

void afficher_proie(proie_t proie)
{
    printf("nom: %d\n", proie.nom);
    printf("coordonnées: (%d , %d)\n", proie.x, proie.y);
    if (proie.vie == 1)
    {
        printf("vivant\n\n");
    }
    else printf("mort\n\n");
}
