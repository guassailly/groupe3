#include "structures.h"
#include "regles.h"
#include "propre.h"


#define RADIUS 25

///////////////////////////////////////////////////////////////////////////////////////////
//FONCTION UTILITAIRES
///////////////////////////////////////////////////////////////////////////////////////////
void end_sdl(char ok,char const* msg, fenetre_t SDL) {                         
    char msg_formated[255];                                            
    int l;                                                     
                                        
    if (!ok) {                                                       
        strncpy(msg_formated, msg, 250);                                         
        l = strlen(msg_formated);                                            
        strcpy(msg_formated + l, " : %s\n");                                                                             
        SDL_Log(msg_formated, SDL_GetError());                                   
    }                                                          
                                        
    if (SDL.renderer != NULL) {                                         
        SDL_DestroyRenderer(SDL.renderer);                                 
        SDL.renderer = NULL;
    }
    if (SDL.window != NULL)   {                                           
        SDL_DestroyWindow(SDL.window);                                    
        SDL.window= NULL;
    }
                                        
    SDL_Quit();                                                    
                                        
    if (!ok) {                                              
        exit(EXIT_FAILURE);                                                  
    }
    
                                                
}     







void afficherTexteSDL(const char* texte, int x, int y, TTF_Font* police, SDL_Renderer* renderer)
{
    SDL_Surface* surfaceTexte = TTF_RenderText_Solid(police, texte, (SDL_Color) { .r = 255, .g = 255, .b = 255, .a = 255 });    
    SDL_Texture* textureTexte = SDL_CreateTextureFromSurface(renderer, surfaceTexte);
    
    int largeurTexte = surfaceTexte->w*0.5;
    int hauteurTexte = surfaceTexte->h*0.5;
    
    SDL_Rect rectDest;
    rectDest.x = x;
    rectDest.y = y;
    rectDest.w = largeurTexte;
    rectDest.h = hauteurTexte;
    
    SDL_RenderCopy(renderer, textureTexte, NULL, &rectDest);
    
    SDL_DestroyTexture(textureTexte);
    SDL_FreeSurface(surfaceTexte);
}


SDL_Texture* textureLoad(char * tex_name, fenetre_t SDL)
{
    SDL_Surface *my_image = NULL;          
    SDL_Texture* my_texture = NULL;         
    my_image = IMG_Load(tex_name);
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", SDL);
    my_texture = SDL_CreateTextureFromSurface(SDL.renderer, my_image);
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", SDL);
    SDL_FreeSurface(my_image);
    return my_texture;
}                                        



void afficherJoueurs(SDL_Renderer* renderer,  monde_t Jeu)
{
    // int i,j;
    // SDL_Rect rect;
    // rect.w = PAS;
    // rect.h = PAS;


    
    // SDL_SetRenderDrawColor(renderer, 64, 64, 64, 255);
    // for(i=0;i<TAILLE+2;i++){
    //     for(j=0;j<TAILLE+2;j++){
    //         if(Jeu.MAT[i][j]!=0){
    //             rect.x=j*PAS;
    //             rect.y=i*PAS;
    //             SDL_RenderFillRect(renderer, &rect);
    //         }
    //     }
    // }


    // SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    // for(i = 0; i <NB_PREDATEURS; i++)
    // {
    //     rect.x=Jeu.tabPredateur[i].x*PAS;
    //     rect.y=Jeu.tabPredateur[i].y*PAS;
    //     SDL_RenderFillRect(renderer, &rect);
    // }

    
    // SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
    // for(i = 0; i <NB_PROIES; i++)
    // {
    //     if(Jeu.tabProie[i].vie!=0){
    //         rect.x=Jeu.tabProie[i].x*PAS;
    //         rect.y=Jeu.tabProie[i].y*PAS;
    //         SDL_RenderFillRect(renderer, &rect);
    //     }
    // }

    // SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);

    // rect.x=Jeu.berger->x*PAS;
    // rect.y=Jeu.berger->y*PAS;
    // SDL_RenderFillRect(renderer, &rect);


    int id = proie_proche(Jeu.tabProie,Jeu.tabPredateur[0]);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderDrawLine(renderer,Jeu.tabPredateur[0].x*PAS+(PAS/2),Jeu.tabPredateur[0].y*PAS+(PAS/2),Jeu.tabProie[id].x*PAS+(PAS/2),Jeu.tabProie[id].y*PAS+(PAS/2));
}


void printTerrain(int ** Terrain){
    int i,j;
    for(i=0;i<(TAILLE+2);i++){
        for(j=0;j<(TAILLE+2);j++){
            printf("%d ",Terrain[i][j]);
        }printf("\n");
        
    }printf("\n");
}



void freeTerrain(int ** Terrain){
    int i;
    for(i=0;i<(TAILLE+2);i++){
        free(Terrain[i]);
    }free(Terrain);
}


int deplacerPredateur(monde_t * Jeu, regle_t * tableRegles, int nbregles,int *tabAux){
    int direction = 0;
    int res = choisirAction(tableRegles, nbregles, *Jeu, 1,tabAux);
    if(res !=-1)
    {
        switch(tableRegles[res].action_a_Realiser)
        {
            case HAUT:
                if(Jeu->tabPredateur[0].y>1 && Jeu->MAT[Jeu->tabPredateur[0].y-1][Jeu->tabPredateur[0].x]==0)
                {
                    Jeu->tabPredateur[0].y-=1;
                }
                direction = 0;
                break;
            case DROITE:
                if(Jeu->tabPredateur[0].x<TAILLE && Jeu->MAT[Jeu->tabPredateur[0].y][Jeu->tabPredateur[0].x+1]==0)
                {
                    Jeu->tabPredateur[0].x+=1;
                }
                direction = 1;
            break;
            case BAS:
                if(Jeu->tabPredateur[0].y<TAILLE && Jeu->MAT[Jeu->tabPredateur[0].y+1][Jeu->tabPredateur[0].x]==0)
                {
                    Jeu->tabPredateur[0].y+=1;
                }
                direction = 2;
            break;
            case GAUCHE:
                if(Jeu->tabPredateur[0].x>1 && Jeu->MAT[Jeu->tabPredateur[0].y][Jeu->tabPredateur[0].x-1]==0)
                {
                    Jeu->tabPredateur[0].x-=1;
                }
                direction = 3;
            break;
        }
    } 
    int idProieProche=proie_proche(Jeu->tabProie,Jeu->tabPredateur[0]);
    if(Jeu->tabProie[idProieProche].vie !=0 && Jeu->tabProie[idProieProche].x == Jeu->tabPredateur[0].x && Jeu->tabProie[idProieProche].y == Jeu->tabPredateur[0].y){
        Jeu->tabProie[idProieProche].vie=0;
        Jeu->score_moutons+=1;
        //printf("Score moutons: %d\n",Jeu->score_moutons);
        //idProieProche=proie_proche(Jeu->tabProie,Jeu->tabPredateur[0]);
    }

    return direction;
}

int deplacerBerger(monde_t Jeu)
{
    int res = 0;
    switch(res=choisirActionBerger(Jeu,VIGILANCE))
    {
        case 0:
            if(Jeu.berger->y>1 && Jeu.MAT[Jeu.berger->y-1][Jeu.berger->x]==0)
                Jeu.berger->y-=1;
        break;
        case 1:
            if(Jeu.berger->x<TAILLE && Jeu.MAT[Jeu.berger->y][Jeu.berger->x+1]==0)
                Jeu.berger->x+=1;
        break;
        case 2: 
            if(Jeu.berger->y<TAILLE && Jeu.MAT[Jeu.berger->y+1][Jeu.berger->x]==0)
                Jeu.berger->y+=1;
        break;
        case 3:
            if(Jeu.berger->x>1 && Jeu.MAT[Jeu.berger->y][Jeu.berger->x-1]==0)
                Jeu.berger->x-=1;
        break;
    }
    return res;
}

int deplacerBergerJoueur(monde_t Jeu,int ordre)
{
    int res = ordre;
    switch(ordre)
    {
        case 0:
            if(Jeu.berger->y>1 && Jeu.MAT[Jeu.berger->y-1][Jeu.berger->x]==0)
                Jeu.berger->y-=1;
        break;
        case 1:
            if(Jeu.berger->x<TAILLE && Jeu.MAT[Jeu.berger->y][Jeu.berger->x+1]==0)
                Jeu.berger->x+=1;
        break;
        case 2: 
            if(Jeu.berger->y<TAILLE && Jeu.MAT[Jeu.berger->y+1][Jeu.berger->x]==0)
                Jeu.berger->y+=1;
        break;
        case 3:
            if(Jeu.berger->x>1 && Jeu.MAT[Jeu.berger->y][Jeu.berger->x-1]==0)
                Jeu.berger->x-=1;
        break;
    }
    return res;
}


void deplacerProies(proie_t * troupeau, predateur_t * meute, int obj_x, int obj_y, int indiceChef)  // Il faut rentrer les coordonnées de l'objectif initial du chef des moutons.
{
    predateur_t loup = meute[0];

    //int indiceChef = reunion_des_moutons(troupeau, RAYON);
    // int chef_x = troupeau[indiceChef].x;
    // int chef_y = troupeau[indiceChef].y;

    float p = 0.65;  //proba que le chef avance dans la direction de l'objectif (sinon c'est aléatoire), p augmente => aleatoire augmente.

    card_t * liste_dir = centre_moutons(troupeau);

    float pif;
    for (int i=0; i<NB_PROIES; ++i)  // Déplacer les moutons plus ou moins aléatoirement.
    {
        pif = ((float)rand()/(float)RAND_MAX);
        if (pif > p)
        {
            troupeau[i].direction=deplacer(liste_dir[i], &(troupeau[i].x), &(troupeau[i].y), troupeau);
            // printf("X%d : %d\n",i, troupeau[i].x);
        }
        else
        {
             troupeau[i].direction=deplacer(direction_aleatoire(), &(troupeau[i].x), &(troupeau[i].y), troupeau);
        }
    }

    if (euclidienne(troupeau[indiceChef].x, troupeau[indiceChef].y, obj_x, obj_y) > 3  )
    {
        // DEPLACEMENT DU CHEF //
        card_t dir_chef = direction_objectif(obj_x, obj_y, troupeau[indiceChef].x, troupeau[indiceChef].y);
        float alea = ((float)rand()/(float)RAND_MAX);
        if (alea > p)
        {
            troupeau[indiceChef].direction=deplacer(dir_chef, &(troupeau[indiceChef].x), &(troupeau[indiceChef].y), troupeau);
        }
        else
        {
            troupeau[indiceChef].direction=deplacer(direction_aleatoire(), &(troupeau[indiceChef].x), &(troupeau[indiceChef].y), troupeau);
        }   
    }
    else  // Objectif quasiment atteint, on change d'objectif en fuyant le loup
    {
        switch (direction_objectif(loup.x, loup.y, troupeau[indiceChef].x, troupeau[indiceChef].y) )
        {
            case N:
                obj_y = TAILLE;
            break;
            case S:
                obj_y = 1;
            break;
            case E:
                obj_x = 1;
            break;
            case O:
                obj_x = TAILLE;
            break;
            default:
                obj_y = TAILLE;
            break;
        }
        // obj_x = rand()%TAILLE;
        // obj_y = rand()%TAILLE;
    }
}



void drawBG(monde_t Jeu, SDL_Texture* text, fenetre_t SDL)
{

    

    SDL_Rect 
        source = {0},                    // Rectangle définissant la zone totale de la planche
        window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
        state = {0};  
    SDL_QueryTexture(text,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);
    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
        &window_dimensions.w,
        &window_dimensions.h);

    for(int i = 0; i <= TAILLE+1; i++)
    {
        for(int j = 0; j <= TAILLE+1; j++)
        {
            
            destination.x=j*PAS;
            destination.y=i*PAS;
            destination.w=PAS;
            destination.h=PAS;
            state.h=source.h;
            state.y=0;
            state.w=source.w/3;
            if(Jeu.MAT[i][j]==0 )
            {
                state.x=32;
            }
            else if(Jeu.MAT[i][j]==1)
            {
                state.x=64;
            }
            else if(Jeu.MAT[i][j]==2)
            {
                state.x=0;
            }
            SDL_RenderCopy(SDL.renderer,text,&state,&destination);
        }
    }
}

void drawLoup(monde_t Jeu, SDL_Texture* text, fenetre_t SDL,int direction)
{

    SDL_Rect 
        source = {0},                    
        window_dimensions = {0},         
        destination = {0},               
        state = {0};  
    SDL_QueryTexture(text,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);
    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
        &window_dimensions.w,
        &window_dimensions.h);

    destination.x=Jeu.tabPredateur[0].x*PAS;
    destination.y=Jeu.tabPredateur[0].y*PAS;
    destination.w=PAS;
    destination.h=PAS;
    switch (direction)
    {
        case 0:
        state.y=249;
        break;
        case 1:
            state.y=166;
        break;
        case 2:
            state.y=0;
        break;
        case 3:
            state.y=83;
        break;
    }
    state.h=source.h/4;

    state.w=source.w;
    state.x=0;
    SDL_RenderCopy(SDL.renderer,text,&state,&destination);
}

void drawBerger(monde_t Jeu, SDL_Texture* text, fenetre_t SDL,int direction)
{

    SDL_Rect 
        source = {0},                    
        window_dimensions = {0},         
        destination = {0},               
        state = {0};  
    SDL_QueryTexture(text,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);
    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
        &window_dimensions.w,
        &window_dimensions.h);

    destination.x=Jeu.berger->x*PAS;
    destination.y=Jeu.berger->y*PAS;
    destination.w=PAS;
    destination.h=PAS;
    switch (direction)
    {
        case 0:
            state.y=3*60;
        break;
        case 1:
            state.y=2*60;
        break;    
        case 2:
            state.y=0;
        break;
        case 3:
            state.y=60;
        break;
    }
    state.h=source.h/4;

    state.w=source.w;
    state.x=0;
    SDL_RenderCopy(SDL.renderer,text,&state,&destination);
}



void drawMouton(monde_t Jeu, SDL_Texture* text, fenetre_t SDL)
{
    SDL_Rect 
        source = {0},                    
        window_dimensions = {0},         
        destination = {0},               
        state = {0};  
    SDL_QueryTexture(text,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);
    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
        &window_dimensions.w,
        &window_dimensions.h);

    state.x=64;
    state.y=0;
    state.w=source.w/4;
    state.h=source.h;

    destination.h=PAS;
    destination.w=PAS;
    for(int i = 0; i<NB_PROIES; i++)
    {
        if(Jeu.tabProie[i].vie!=0)
        {
            destination.x = Jeu.tabProie[i].x*PAS;
            destination.y = Jeu.tabProie[i].y*PAS;
            switch(Jeu.tabProie[i].direction)
            {
                case 0:
                    state.x=0;
                break;
                case 1:
                    state.x=3*64;
                break;
                case 2:
                    state.x=2*64;
                break;
                case 3:
                    state.x=64;
                break;

            }

        SDL_RenderCopy(SDL.renderer,text,&state,&destination);
        }

    }    

}


void drawBarre2Loup(monde_t Jeu, fenetre_t SDL, SDL_Texture* textureLoup)
{   
    SDL_Rect 
        source = {0},                    
        window_dimensions = {0},         
        destination = {0},               
        state = {0};  
    SDL_QueryTexture(textureLoup,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);
    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
        &window_dimensions.w,
        &window_dimensions.h);
    destination.y=(TAILLE+2)*PAS +30;
    destination.w=40;
    destination.h=40;
    state.y=0;
    state.x=0;
    state.h=source.h/4;
    state.w=source.w;

    int nbLoupAAfficher = 5-Jeu.score_berger;

    for(int i = 0; i < nbLoupAAfficher; i++)
    {
        destination.x = 25 * i + 10;
        SDL_RenderCopy(SDL.renderer,textureLoup,&state,&destination);

    }
}


void drawBarre2Moutons(monde_t Jeu, fenetre_t SDL, SDL_Texture* textureMouton)
{
    SDL_Rect 
        source = {0},                    
        window_dimensions = {0},         
        destination = {0},               
        state = {0};  
    SDL_QueryTexture(textureMouton,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);
    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
        &window_dimensions.w,
        &window_dimensions.h);
    destination.y=(TAILLE+2)*PAS +30;
    destination.w=40;
    destination.h=40;
    state.y=0;
    state.x=64;
    state.h=source.h;
    state.w=source.w/4;

    int nbMoutonAAfficher = NB_PROIES-Jeu.score_moutons;

    for(int i = 0; i < nbMoutonAAfficher; i++)
    {
        destination.x = (TAILLE*PAS+30)-i*20;
        SDL_RenderCopy(SDL.renderer,textureMouton,&state,&destination);

    }
}

void drawBotButton(fenetre_t SDL, SDL_Texture* textureBot,int isAutomated)
{
    SDL_Rect 
        source = {0},                    
        destination = {0},               
        state = {0};  
    SDL_QueryTexture(textureBot,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);

    destination.y = 0;
    destination.x=(TAILLE+1)*PAS;
    destination.w=PAS;
    destination.h=PAS;
    state.y=0;
    state.x=0;
    state.h=source.h;
    state.w=source.w;
    if(isAutomated)
    {
        SDL_SetTextureColorMod(textureBot,0,255,0);
    }
    else {
        SDL_SetTextureColorMod(textureBot,255,255,255);
    }
    SDL_RenderCopy(SDL.renderer,textureBot,&state,&destination);
    
}



void gameLoop(fenetre_t SDL)
{
    SDL_bool program_on = SDL_TRUE;          
    SDL_Event event;



    float pourcentVictoire = 0;
    int directionLoup = 0;
    int directionBerger = 0;
    
    int nbPartie = 1;
    monde_t Jeu=initPartie();

    int obj_x = rand()%TAILLE+2;
    int obj_y = rand()%TAILLE+2;

    int isAutomated = 0;

    int hasPlayed = 0;
    FILE * fichier = fopen("regles.txt","rw");

    regle_t* tab = lireRegle(fichier,NB_REGLES);
    
    //afficherRegle(tab,NB_REGLES);
    SDL_Texture* bg = textureLoad("tex/terrain.png",SDL);
    SDL_Texture* loup = textureLoad("tex/loupsprite.png",SDL);
    SDL_Texture* mouton = textureLoad("tex/moutonssheet.png",SDL);
    SDL_Texture * berger = textureLoad("tex/berger.png",SDL);
    SDL_Texture * bot = textureLoad("tex/bot.png", SDL);


    TTF_Init();
    TTF_Font* police = TTF_OpenFont("Pixel Digivolve.ttf", 35);


    int *tabAux = malloc(sizeof(int)*NB_REGLES);

    while (program_on == SDL_TRUE) {
      

        hasPlayed = 0;
        if(GRAPHIC)
        {
            SDL_SetRenderDrawColor(SDL.renderer,47,129,54,255);      
            SDL_RenderClear(SDL.renderer);
            drawBG(Jeu,bg,SDL);
            afficherJoueurs(SDL.renderer,Jeu);
            drawMouton(Jeu,mouton,SDL);
            drawLoup(Jeu,loup,SDL,directionLoup);
            drawBerger(Jeu,berger,SDL,directionBerger);
            drawBarre2Loup(Jeu,SDL,loup);
            drawBarre2Moutons(Jeu,SDL,mouton);
            drawBotButton(SDL,bot,isAutomated);
            char txtPartie[30];
            sprintf(txtPartie,"Partie : %d",nbPartie);
            afficherTexteSDL(txtPartie,10,(TAILLE+2)*PAS +5,police,SDL.renderer);
            char txtPourcent[30];
            sprintf(txtPourcent,"Victoire du Berger : %.2f %%",pourcentVictoire/nbPartie*100);
            afficherTexteSDL(txtPourcent,(TAILLE-3.4)*PAS,(TAILLE+2)*PAS +5,police,SDL.renderer);

            SDL_RenderPresent(SDL.renderer);  
        }

 
        int indiceChef = reunion_des_moutons(Jeu.tabProie, RAYON);
        deplacerProies(Jeu.tabProie, Jeu.tabPredateur, obj_x, obj_y, indiceChef);
        directionLoup=deplacerPredateur(&Jeu,tab,NB_REGLES,tabAux);
        

        if(Jeu.score_moutons==NB_PROIES || Jeu.etape >= LIMITEPARTIE || Jeu.score_berger>=5){
            printf("%d %d %d\n",Jeu.score_berger,Jeu.score_moutons, Jeu.etape);
            if(Jeu.score_berger>=5)
            {
                pourcentVictoire += 1;
            }
            nbPartie++;
            afficherTabRegleUtilisée(tabAux);
            Jeu=initPartie();
        }
        Jeu.etape++;


        while(!hasPlayed)
        {
  
            if (SDL_PollEvent(&event)!=0 || event.type!=SDL_MOUSEMOTION) {
                switch(event.type ) {
                    case SDL_QUIT:
                        program_on = SDL_FALSE;
                    break;
                    case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                            case SDLK_UP:
                            if(!isAutomated)
                            {
                                directionBerger= deplacerBergerJoueur(Jeu,0);
                                hasPlayed = 1;
                            }
                            break;
                            case SDLK_RIGHT:
                            if(!isAutomated)
                            {
                                directionBerger= deplacerBergerJoueur(Jeu,1);
                                hasPlayed = 1;
                            }
                            break;
                              case SDLK_DOWN:
                              if(!isAutomated)
                              {
                                directionBerger= deplacerBergerJoueur(Jeu,2);
                                hasPlayed = 1;
                              }
                            break;
                            case SDLK_LEFT:
                                if(!isAutomated)
                                {
                                    directionBerger= deplacerBergerJoueur(Jeu,3);
                                    hasPlayed = 1;
                                }
                            break;
                        }
                    break;

                    case SDL_MOUSEBUTTONDOWN:
                        if(event.button.button==SDL_BUTTON_LEFT)
                        {
                            if(event.button.x < (TAILLE+2)*PAS && event.button.x > (TAILLE+1)*PAS && event.button.y > 0 && event.button.y < PAS)
                            {
                                printf("%d %d\n",event.button.x,event.button.y);
                                hasPlayed=1;
                                isAutomated = !isAutomated;
                            }             
                        }
                    break;    
                }     
            }
            if(isAutomated)
            {
                directionBerger = deplacerBerger(Jeu);
                SDL_Delay(50);
                hasPlayed = 1;
            }


        }
     
        
      
        if(Jeu.berger->x == Jeu.tabPredateur[0].x && Jeu.berger->y == Jeu.tabPredateur[0].y){
            Jeu.score_berger+=1;
            printf("Score berger : %d\n",Jeu.score_berger);
            Jeu.berger->x=rand()%TAILLE+1;
            Jeu.berger->y=rand()%TAILLE+1;
            Jeu.tabPredateur[0].x=rand()%TAILLE+1;
            Jeu.tabPredateur[0].y=rand()%TAILLE+1;

            if(!isAutomated)
            {
                SDL_SetRenderDrawColor(SDL.renderer,255,0,0,255);
                SDL_Rect rect;
                rect.h=50;  
                rect.w=50;
                rect.x=TAILLE; 
                rect.y=TAILLE;  
                SDL_RenderClear(SDL.renderer);
                SDL_RenderFillRect(SDL.renderer,&rect);
                SDL_RenderPresent(SDL.renderer); 
                SDL_Delay(50); 
            }

          
        }
    }

    freeTerrain(Jeu.MAT);
    TTF_CloseFont(police);
    TTF_Quit();
    free(tabAux);

}




    
int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    fenetre_t SDL;
    SDL.renderer=NULL;
    SDL.window=NULL;

    srand(time(NULL));
    
    SDL_DisplayMode screen; SDL.renderer = SDL_CreateRenderer(SDL.window, -1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    /*********************************************************************************************************************/  
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", SDL);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
        screen.w, screen.h);

    /* Création de la fenêtre */
    SDL.window = SDL_CreateWindow("Sheep qui peut!",
                0,
                0, 
                PAS*(TAILLE+2),
                PAS*(TAILLE+2)+100,
                SDL_WINDOW_OPENGL);
    if (SDL.window == NULL) end_sdl(0, "ERROR WINDOW CREATION", SDL);

    /* Création du renderer */
    SDL.renderer = SDL_CreateRenderer(SDL.window, -1,
                    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (SDL.renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", SDL);
    // gameLoop(SDL);


    gameLoop(SDL);

    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", SDL);
    return EXIT_SUCCESS;
}