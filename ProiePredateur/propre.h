#ifndef PROPRE_H
#define PROPRE_H

#include<stdio.h>
#include<stdlib.h>
#include <time.h>
#include "voyageur.h"


#define NB_PREDATEURS 1
#define NB_PROIES 10
#define TAILLE 10
#define PAS 50
#define VIGILANCE 0.7
#define RAYON 2
#define LIMITEPARTIE 1000
#define GRAPHIC 1
#define PROBA_OBSTACLE 0.05


typedef struct {
    int nom;
    int x;    // coordonnée sur x (horizontale)
    int y;    // coordonnée sur y (verticale)
    int vie;  // vie = 0 --> mort ; vie = 1 --> vivant
    //...
    int nom_ami;  // numéro de l'ami prédateur le plus proche
    int nom_proie;
    int dist_proie; // proie proche: 0; loin: 1
}predateur_t;

typedef struct  {
    int nom;
    int x;
    int y;
    int vie;

    int direction; 

    int nom_pred;  //nom du predateur le plus proche.
    int dist_pred; // proche: 0; loin: 1
}proie_t;

typedef struct {
    int x;
    int y;
}berger_t;

predateur_t * init_pred();

proie_t * init_proie();

berger_t * init_berger();

void afficher_pred(predateur_t pred);

void init_obstacles(proie_t * tableau_proies,int ** Matrice);

void afficher_proie(proie_t proie);

int proie_proche(proie_t * liste_proie, predateur_t pred);

#endif