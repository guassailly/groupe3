#ifndef REGLES_H
#define REGLES_H
#include <stdio.h>
#include <stdlib.h>
#include "propre.h"


#define NB_REGLES 40

typedef enum {
 JOKERCASE=-1,VIDE=0,OBSTACLE,MUR
} case_t;



typedef enum{
    JOKERDIR=-1,N,E,S,O
} card_t;


typedef enum{
    JOKERDIST = -1,PRES,LOIN,HC
}distance_t;

typedef enum {
    HAUT,DROITE,BAS,GAUCHE
} action_t;

typedef struct {
    case_t devant,droite,derrière,gauche;
    card_t direction_proie;
    distance_t distance_proie;
    card_t direction_def;
    distance_t distance_def;
    action_t action_a_Realiser;
    int priorite;
} regle_t;

typedef struct {
    case_t devant,droite,derrière,gauche;
    card_t direction_proie;
    distance_t distance_proie;
    card_t direction_def;
    distance_t distance_def;
} observation_t;



typedef struct {
    proie_t * tabProie;
    predateur_t * tabPredateur;
    berger_t* berger;
    int ** MAT;
    int score_moutons;
    int score_berger;
    int etape;
}monde_t;


regle_t* lireRegle(FILE* fic,int nbRegles);

void afficherRegle(regle_t * tab, int nbRegles);

int ** initTerrain(int n, double proba, monde_t jeu);

monde_t initPartie();

void afficherObservation(observation_t obs);

int choisirAction(regle_t * tabRegle, int nbRegle, monde_t jeu, int s, int *tabAux);

int choisirActionBerger(monde_t jeu,float p);



int reunion_des_moutons(proie_t * troupeau, int rayon);

card_t * centre_moutons(proie_t * troupeau);


card_t direction_objectif(int obj_x, int obj_y, int chef_x, int chef_y);

int deplacer(card_t dir, int* animal_x, int* animal_y, proie_t *troupeau);


card_t direction_aleatoire();


int colision(proie_t *troupeau, int animal_x, int animal_y);

void afficherTabRegleUtilisée(int * tab);


#endif