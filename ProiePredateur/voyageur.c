#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<math.h>
#include "voyageur.h"

int** allouer(int n)
{
    int** matrice = (int **)malloc(n * sizeof(int *));
    for (int i = 0; i < n; i++) {
        matrice[i] = (int *)malloc(n * sizeof(int));
        for (int j = 0; j < n; j++) {
            matrice[i][j] = 0;
        }
    }
    return matrice;
}

void generer(int** matrice, int bas, int haut)
{
    srand(time(NULL));
    if (bas < haut) {
        int k = (rand() % (haut - bas)) + (bas + 1);
        matrice[bas][bas + 1] = 1;
        matrice[bas + 1][bas] = 1;
        if (k + 1 <= haut) {
            matrice[bas][k + 1] = 1;
            matrice[k + 1][bas] = 1;
        }
        generer(matrice, bas + 1, k);
        generer(matrice, k + 1, haut);
    }
}

void liberer(int** matrice, int n) {
    for (int i = 0; i < n; i++) {
        free(matrice[i]);
    }
    free(matrice);
}

void afficher(int** matrice, int n)
{
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            printf("%d ", matrice[i][j]);
        }
        printf("\n");
    }
}

// int euclidienne(point_t point1, point_t point2)
// {
//     int dx, dy;
//     dx = point1.x - point2.x;
//     dy = point1.y - point2.y;
//     return sqrt(pow(dx,2) + pow(dy,2));
// }

int euclidienne(int x1, int y1, int x2, int y2)
{
    int dx, dy;
    dx = x1 - x2;
    dy = y1 - y2;
    return sqrt(pow(dx,2) + pow(dy,2));
}

void genererGraphe(int** M,int taille, float p)
{
    srand(time(NULL));
    for (int i=0; i<taille; ++i)
    {
        for (int j=i+1; j<taille; ++j)
        {
            if ((float)rand() /RAND_MAX < p)
            {
                M[i][j]=1;
                M[j][i]=1;
            }
        }
    }
}