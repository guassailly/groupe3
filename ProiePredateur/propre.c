#include<stdio.h>
#include<stdlib.h>
#include "propre.h"

predateur_t * init_pred()
{
    predateur_t * liste_pred=malloc(NB_PREDATEURS*sizeof(predateur_t));
    for (int i=0;i<NB_PREDATEURS;++i)
    {
        liste_pred[i].nom = i;
        liste_pred[i].x = (rand()% TAILLE)+1;  // pour un  terrain de 1200*800px en se deplaçant tous les 50px
        liste_pred[i].y = (rand()% TAILLE)+1;
        liste_pred[i].vie = 1;              // tous les animaux sont vivants au départ.
    }
    return liste_pred; 
}

proie_t * init_proie()
{
    
    proie_t * liste_proie=malloc(NB_PROIES*sizeof(proie_t));
    for (int i=0;i<NB_PROIES;++i)
    {
        liste_proie[i].nom = i;
        liste_proie[i].x = (rand()% TAILLE)+1;  // pour un  terrain de 1200*800px en se deplaçant tous les 50px
        liste_proie[i].y = (rand()% TAILLE)+1;
        liste_proie[i].vie = 1;              // tous les animaux sont vivants au départ.//
    }
    return liste_proie;
}



berger_t * init_berger(){
    berger_t* berger = malloc(sizeof(berger_t));
    berger->x = (rand()% TAILLE)+1;
    berger->y = (rand()% TAILLE)+1;
    return berger;
}


void afficher_pred(predateur_t pred)
{
    printf("nom: %d\n", pred.nom);
    printf("coordonnées: (%d , %d)\n", pred.x, pred.y);
    if (pred.vie == 1)
    {
        printf("vivant\n\n");
    }
    else printf("mort\n\n");
}

void afficher_proie(proie_t proie)
{
    printf("nom: %d\n", proie.nom);
    printf("coordonnées: (%d , %d)\n", proie.x, proie.y);
    if (proie.vie == 1)
    {
        printf("vivant\n\n");
    }
    else printf("mort\n\n");
}

int proie_proche(proie_t * liste_proie, predateur_t pred)
{ 
    int min = 0;
    for (int i=0; i<NB_PROIES; i++)
    {
        if (((euclidienne(liste_proie[i].x, liste_proie[i].y, pred.x, pred.y) < euclidienne(pred.x, pred.y, liste_proie[min].x, liste_proie[min].y)) 
        && liste_proie[i].vie != 0)
        || (liste_proie[min].vie == 0 && liste_proie[i].vie==1))
        {   
            min = i;
        }
    }
    return min;
}