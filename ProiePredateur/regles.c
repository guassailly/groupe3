
#include "regles.h"

regle_t* lireRegle(FILE* fic,int nbRegles)
{

    regle_t* tab = malloc(sizeof(regle_t) * nbRegles);
    for(int i = 0; i<nbRegles; i++)
    {
        int result = fscanf(fic,"[%d %d %d %d; %d %d; %d %d] -> %d (%d)\n",(int *)&(tab[i].devant),(int*)&(tab[i].droite),
                                                            (int *)&(tab[i].derrière),(int*)&(tab[i].gauche),
                                                            (int *)&(tab[i].direction_proie),(int*)&(tab[i].distance_proie),
                                                            (int *)&(tab[i].direction_def),(int*)&(tab[i].distance_def),
                                                            (int *)&(tab[i].action_a_Realiser),&(tab[i].priorite));
        if(result!=10) {
            printf("erreur\n");
        }
    }
    return tab;
}

void afficherRegle(regle_t * tab, int nbRegles)
{  
    for(int i = 0; i<nbRegles; i++)
    {
        printf("[%d %d %d %d; %d %d; %d %d] -> %d (%d)\n",tab[i].devant,tab[i].droite,tab[i].derrière,tab[i].gauche,tab[i].direction_proie,tab[i].distance_proie,tab[i].direction_def,tab[i].distance_def,tab[i].action_a_Realiser,tab[i].priorite);
    }
} 




int ** initTerrain(int n, double proba, monde_t jeu){
    int ** Terrain=(int**)malloc((n+2)*sizeof(int*));
    int i,j;
    for(i=0;i<(n+2);i++){
        Terrain[i]=(int*)malloc((n+2)*sizeof(int));
    }
    double obstacle;

    for(i=0;i<(n+2);i++){
        for(j=0;j<(n+2);j++){
            if(j==(n+1) || i==(n+1) || j==0 || i==0){
                Terrain[i][j]=2;
            } else {
                Terrain[i][j]=0;
                obstacle=((double)rand()/(double)RAND_MAX);
                if(obstacle>proba){
                    Terrain[i][j]=0;
                } else {
                    int verif = 1;
                    for(int k=0;k<NB_PROIES;k++){
                        if((jeu.tabProie[k].x==j 
                        && jeu.tabProie[k].y==i)
                        ||(jeu.tabPredateur[0].x==j
                        &&jeu.tabPredateur[0].y==i)
                        ||(jeu.berger->x==j
                        &&jeu.berger->y==i))verif=0;
                    }
                    if(verif)Terrain[i][j]=1;
                }
                
            }
        }
    }
    return Terrain;
}



monde_t initPartie()
{
    monde_t jeu;
    jeu.etape = 0;
    jeu.score_berger=0;
    jeu.score_moutons=0;
    jeu.tabProie=init_proie();
    jeu.berger=init_berger(jeu);
    jeu.tabPredateur=init_pred();
    jeu.MAT = initTerrain(TAILLE,PROBA_OBSTACLE,jeu);


    return jeu;
}


int choisirActionBerger(monde_t jeu,float p)
{
    int res;
    if((float)rand()/RAND_MAX < p)
    {

        if(jeu.tabPredateur[0].y -jeu.berger->y>=abs(jeu.tabPredateur[0].x-jeu.berger->x))
        {
            res = 2;
        }
        if(jeu.berger->y -jeu.tabPredateur[0].y>=abs(jeu.tabPredateur[0].x-jeu.berger->x))
        {
           res = 0;
        }
        if(jeu.berger->x -jeu.tabPredateur[0].x>=abs(jeu.tabPredateur[0].y-jeu.berger->y))
        {
            res = 3;
        }
        if(jeu.tabPredateur[0].x -jeu.berger->x>=abs(jeu.tabPredateur[0].y-jeu.berger->y))
        {
            res = 1;
        }
    }    
    else {
        res = rand()%4;
    }    
    return res;
}


observation_t Observateur(monde_t jeu)
{

    observation_t obs;



    obs.devant=jeu.MAT[jeu.tabPredateur[0].x][jeu.tabPredateur[0].y-1];
    obs.droite=jeu.MAT[jeu.tabPredateur[0].x+1][jeu.tabPredateur[0].y];
    obs.derrière=jeu.MAT[jeu.tabPredateur[0].x][jeu.tabPredateur[0].y+1];
    obs.gauche=jeu.MAT[jeu.tabPredateur[0].x-1][jeu.tabPredateur[0].y];
   
    int proieProche = proie_proche(jeu.tabProie,jeu.tabPredateur[0]);

    //OBSERVER PROIES
    if(jeu.tabPredateur[0].y -jeu.tabProie[proieProche].y>=abs(jeu.tabPredateur[0].x-jeu.tabProie[proieProche].x))
    {
        obs.direction_proie=N;
    }
    if(jeu.tabProie[proieProche].y -jeu.tabPredateur[0].y>=abs(jeu.tabPredateur[0].x-jeu.tabProie[proieProche].x))
    {
        obs.direction_proie=S;
    }
    if(jeu.tabProie[proieProche].x -jeu.tabPredateur[0].x>=abs(jeu.tabPredateur[0].y-jeu.tabProie[proieProche].y))
    {
        obs.direction_proie=E;
    }
    if(jeu.tabPredateur[0].x -jeu.tabProie[proieProche].x>=abs(jeu.tabPredateur[0].y-jeu.tabProie[proieProche].y))
    {
        obs.direction_proie=O;
    }


    //A CHANGER !!!!!
    if(euclidienne(jeu.tabPredateur[0].x,jeu.tabPredateur[0].y,jeu.tabProie[proieProche].x,jeu.tabProie[proieProche].y) > 5)
    {
        obs.distance_proie=LOIN;
    } else
    {
        obs.distance_proie=PRES;
    }

    //OBSERVER 
    if(jeu.tabPredateur[0].y -jeu.berger->y>=abs(jeu.tabPredateur[0].x-jeu.berger->x))
    {
        obs.direction_def=N;
    }
    if(jeu.berger->y -jeu.tabPredateur[0].y>=abs(jeu.tabPredateur[0].x-jeu.berger->x))
    {
        obs.direction_def=S;
    }
    if(jeu.berger->x -jeu.tabPredateur[0].x>=abs(jeu.tabPredateur[0].y-jeu.berger->y))
    {
        obs.direction_def=E;
    }
    if(jeu.tabPredateur[0].x -jeu.berger->x>=abs(jeu.tabPredateur[0].y-jeu.berger->y))
    {
        obs.direction_def=O;
    }


    //A CHANGER !!!!!
    if(euclidienne(jeu.tabPredateur[0].x,jeu.tabPredateur[0].y,jeu.berger->x,jeu.berger->y) > 2)
    {
        obs.distance_def=LOIN;
    } else
    {
        obs.distance_def=PRES;
    }
   
    return obs;
}

void afficherObservation(observation_t obs)
{
    printf("[%d %d %d %d; %d %d; %d %d]\n",obs.devant,obs.droite,obs.derrière,obs.gauche,obs.direction_proie,obs.distance_proie,obs.distance_def,obs.direction_def);
}

int compareAction(observation_t regle, observation_t obs)
{


   return(  (obs.derrière == regle.derrière || regle.derrière == JOKERCASE)
        && (obs.devant == regle.devant || regle.devant == JOKERCASE)
        && (obs.gauche == regle.gauche || regle.gauche == JOKERCASE)
        && (obs.droite == regle.droite || regle.droite == JOKERCASE)
        && (obs.direction_def == regle.direction_def || regle.direction_def == JOKERDIR)
        && (obs.direction_proie == regle.direction_proie || regle.direction_proie == JOKERDIR)
        && (obs.distance_proie == regle.distance_proie || regle.distance_proie == JOKERDIST)
        && (obs.distance_def == regle.distance_def || regle.distance_def == JOKERDIST) 
   );
}

observation_t recupererObservation(regle_t regle)
{
    observation_t obs;
    obs.derrière = regle.derrière;
    obs.devant = regle.devant;
    obs.droite = regle.droite;
    obs.gauche = regle.gauche;
    obs.direction_def = regle.direction_def;
    obs.direction_proie = regle.direction_proie;
    obs.distance_proie = regle.distance_proie;
    obs.distance_def = regle.distance_def;
    return obs;
}




int choisirAction(regle_t * tabRegle, int nbRegle, monde_t jeu, int s, int *tabAux)
{
    
    int regles[nbRegle*2]; //Tableau qui contient une alternance de l'indice de la regle et son poids
    int step = 0;
    int total = 0;
    observation_t obs = Observateur(jeu);
    //afficherObservation(obs);
    for(int i = 0; i < nbRegle; i++)
    {
        observation_t regle = recupererObservation(tabRegle[i]);
        if(compareAction(regle,obs))
        {
            regles[step] = i;
            step++;
            regles[step] = tabRegle[i].priorite;
            step++;
            total+= pow(tabRegle[i].priorite,s);
            //afficherObservation(regle);
        }   
    }
    float p =((float)rand()/(float)RAND_MAX);
    float cumul = 0;
    for(int i = 0; i < step*2; i+=2)
    {
        cumul+=pow(regles[i+1],s)/total; 
        //On ajoute le poids;
        if(p<cumul)
        {
            tabAux[i]=1;
            return regles[i];
        }
    }
    return -1; 

}

void afficherTabRegleUtilisée(int * tab)
{
        printf("Regles Non Utilisées\n");
    for(int i=0; i<NB_REGLES; i++)
    {
        if(tab[i]==0)
        {
            printf("%d ",i);
        }
        
    }
    printf("\n");
}


int reunion_des_moutons(proie_t * troupeau, int rayon)     // retourne le nom du mouton centre
{
    int centre = 1;   // indice du mouton centre
    int m, m_max = 1;
    for (int i=0; i< NB_PROIES; ++i)
    {
        m = 0;
        for (int j=0; j<NB_PROIES; ++j)
        {
            if (euclidienne(troupeau[j].x, troupeau[j].y, troupeau[centre].x, troupeau[centre].y) < rayon && troupeau[j].vie ==1  && troupeau[i].vie == 1)
            {
                m++;   // S'il y a un mouton dans le rayon alors on augmente le nombre de moutons réunis.
            }
        }
        if (m > m_max)
        {
            m_max = m;
            centre = i;
        }
    }
    return centre;
}

card_t * centre_moutons(proie_t * troupeau) // tableau qui contient pour chaque mouton la direction du mouton centre indexé sur chaque mouton.
{
    int centre_x = troupeau[reunion_des_moutons(troupeau, RAYON)].x;
    int centre_y = troupeau[reunion_des_moutons(troupeau, RAYON)].y;
    card_t* dir_centre=malloc(sizeof(card_t) * NB_PROIES);
    
    for (int i=0; i<NB_PROIES; ++i)
    {
        if(troupeau[i].y - centre_y > abs(troupeau[i].x - centre_x))
        {
            dir_centre[i]=N;
        }
        else if(centre_y - troupeau[i].y > abs(troupeau[i].x - centre_x))
        {
            dir_centre[i]=S;
        }
        else if(centre_x - troupeau[i].x >= abs(troupeau[i].y - centre_y))
        {
            dir_centre[i]=E;
        }
        else if(troupeau[i].x - centre_x >= abs(troupeau[i].y - centre_y))
        {
            dir_centre[i]=O;
        }
    }
    return dir_centre;
}

card_t direction_objectif(int obj_x, int obj_y, int chef_x, int chef_y)  // Localisation du chef de troupeau (qui est le centre) et de son objectif.
{
    card_t obj = N;
    if(chef_y - obj_y >= abs(chef_x - obj_x))
    {
        obj=N;
    }
    else if(obj_y - chef_y >= abs(chef_x - obj_x))
    {
        obj=S;
    }
    else if(obj_x - chef_x >= abs(chef_y - obj_y))
    {
        obj=E;
    }
    else if(chef_x - obj_x >= abs(chef_y - obj_y))
    {
        obj=O;
    }
    return obj;
}

int colision(proie_t *troupeau, int animal_x, int animal_y)
{
    int res = 1;
    for (int i=0; i< NB_PROIES; ++i)
    {
        if (animal_y == troupeau[i].y && animal_x == troupeau[i].x)
        {
            res = 0;
            // printf("II(animal_y , troupeau[%d].y) = ((%d, %d))\n", i, animal_y, troupeau[i].y);
            // printf("II(animal_x , troupeau[%d].x) = ((%d, %d)) \n\n", i, animal_x, troupeau[i].x);
        }
    }
    return res;
}

int deplacer(card_t dir, int* animal_x, int* animal_y, proie_t *troupeau)  // Déplace un animal dans la direction souhaitée donnée par l'énumération card.
{
    // printf("ANIMAL_x_y (%d, %d)\n", *animal_x, *animal_y);
    int res = rand()%4;
    int val_x=*animal_x;
    int val_y=*animal_y;
    switch (dir)
    {
        case N:
            if(*animal_y-1 > 0 && colision(troupeau, val_x, val_y-1 ) ==1)
            {
                *animal_y = *animal_y - 1;
                res = 0;
            }
        break;
        case S:
            if(*animal_y+1 < TAILLE && colision(troupeau, val_x, val_y+1 ) ==1)
            {
                *animal_y = *animal_y +1;
                res=1;
            }
        break;
        case E:
            if(*animal_x+1 < TAILLE && colision(troupeau, val_x+1 , val_y) ==1)
            {
                *animal_x = *animal_x +1;
                res=2;
            }
        break;
        case O:
            if(*animal_x-1 > 0 && colision(troupeau, val_x-1 , val_y) ==1)
            {
                *animal_x = *animal_x -1;
                res=3;
            }
        break;
        default :
            // printf("Default X2 : %d\n",*animal_x);
        break;
    }

    return res;
}
 
card_t direction_aleatoire()
{
    card_t dir=N;
    int pouf = rand()%4;
    switch (pouf)
    {
        case 0:
            dir = N;
        break;
        case 1:
            dir = S;
        break;
        case 2:
            dir = E;
        break;
        case 3:
            dir = O;
        break;
    }
    return dir;
}

