#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "SDL2/SDL_ttf.h"
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>


typedef struct coord{
    int x;
    int y;
}coord_t;

typedef struct fenetre{
    SDL_Window * window;
    SDL_Renderer * renderer;
}fenetre_t;


coord_t init_coords(int x, int y);



#endif