#ifndef __include_voyageur_h__
#define __include_fichier_h__

#include<stdio.h>
#include <SDL2/SDL.h>

typedef struct point{
    char nom;
    int x;
    int y;
    int marque;
    int actif;
}point_t;

typedef struct step{
    int indicePoint;
    int distance;
}step_t;


int** allouer(int n);

void generer(int** matrice, int bas, int haut);

void liberer(int** matrice, int n);

void afficher(int** matrice, int n);

//int euclidienne(point_t point1, point_t point2);
int euclidienne(int x1, int y1, int x2, int y2);

void genererGraphe(int** M, int taille, float p);

#endif