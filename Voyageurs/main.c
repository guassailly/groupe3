#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "SDL2/SDL_ttf.h"
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "voyageur.h"


#define RADIUS 20
///////////////////////////////////////////////////////////////////////////////////////////
//FONCTION UTILITAIRES
///////////////////////////////////////////////////////////////////////////////////////////
void end_sdl(char ok,char const* msg,SDL_Window* window,SDL_Renderer* renderer) {                         
    char msg_formated[255];                                            
    int l;                                                     
                                        
    if (!ok) {                                                       
        strncpy(msg_formated, msg, 250);                                         
        l = strlen(msg_formated);                                            
        strcpy(msg_formated + l, " : %s\n");                                                                             
        SDL_Log(msg_formated, SDL_GetError());                                   
    }                                                          
                                        
    if (renderer != NULL) {                                         
        SDL_DestroyRenderer(renderer);                                 
        renderer = NULL;
    }
    if (window != NULL)   {                                           
        SDL_DestroyWindow(window);                                    
        window= NULL;
    }
                                        
    SDL_Quit();                                                    
                                        
    if (!ok) {                                              
        exit(EXIT_FAILURE);                                                  
    }                                                          
}                                                        

void drawCircles(SDL_Renderer* renderer, point_t* listePoints, int nbpoints,int lastClicked)
{
    int tempx = 0;
    int tempy = 0;
    for(int i = 0; i <nbpoints; i++)
    {
        tempx=listePoints[i].x;
        tempy=listePoints[i].y;
        if(listePoints[i].marque==0)
        {
            filledCircleColor(renderer,tempx,tempy,RADIUS,0xFF0000FF);
        }
        else if(i==lastClicked)
        {
            filledCircleColor(renderer,tempx,tempy,RADIUS,0xFF999999);

        }
        else {
            filledCircleColor(renderer,tempx,tempy,RADIUS,0xFFFF0000);
        }
    }


}



void drawGraph(SDL_Renderer* renderer, int** matAdj, point_t* listePoints, int nbpoints,int lastClicked,step_t* chemin,int etape)
{

    

    drawCircles(renderer,listePoints,nbpoints,lastClicked);
    SDL_SetRenderDrawColor(renderer,0,255,0,255);      
    
    // for(int i = 0; i < nbpoints; i++)
    // {
    //     for(int j = 0; j<nbpoints; j++)
    //     {
    //         if(matAdj[i][j]==1)
    //         {
    //             if(listePoints[i].marque==1 && listePoints[j].marque==1)
    //             {
    //                 SDL_SetRenderDrawColor(renderer,0,0,255,255);      
    //             }
    //             else {
    //                 SDL_SetRenderDrawColor(renderer,0,255,0,255);      
    //             }
    //                 SDL_RenderDrawLine(renderer, listePoints[i].x, listePoints[i].y, listePoints[j].x, listePoints[j].y);
    //         }
    //     }
    // }

    ///ON TRACE LES ARCS :

    //TOUT LES ARCS :
    int step = 0;
    for(int i = 0; i < nbpoints; i++)
    {
        for(int j = 0; j<nbpoints; j++)
        {
            if(matAdj[i][j]==1)
            {
                    SDL_SetRenderDrawColor(renderer,0,255,0,255);      
                    SDL_RenderDrawLine(renderer, listePoints[i].x, listePoints[i].y, listePoints[j].x, listePoints[j].y);
            }
        }
    }

    //ARCS PARCOURUS :
    while ((step+1)<=etape)
    {
        SDL_SetRenderDrawColor(renderer,255,0,255,255);      
        SDL_RenderDrawLine(renderer, listePoints[chemin[step].indicePoint].x,listePoints[chemin[step].indicePoint].y, listePoints[chemin[step+1].indicePoint].x, listePoints[chemin[step+1].indicePoint].y);
        step++;
    }
    
    


}

void CircleClicked(int mouseX, int mouseY, point_t* listePoints, int** matAdj, int nbpoints,int* lastClicked,step_t *chemin,int* etape)
{
    

    for(int i = 0; i< nbpoints; i++)
    {
        if(euclidienne(mouseX,mouseY,listePoints[i].x,listePoints[i].y) < RADIUS)
        {

            //SOIT : 

            // // On vérifie que le point cliqué est lié à un autre déjà marqué
            // for(int k = 0; k < nbpoints; k++)
            // {
            //     if(listePoints[k].marque== 1 && matAdj[i][k]==1)
            //     {
                    

            //         *lastClicked = i;
            //         listePoints[i].marque = (listePoints[i].marque+1)%2;
            //         break;
            //     }
            // }

            //On vérifie plutôt qu'il est relié au dernier point cliqué
            if(matAdj[i][*lastClicked]==1 || i==*lastClicked)
            {
                if(listePoints[i].marque ==0) listePoints[i].marque=1;
                *etape=*etape+1;
                chemin[*etape].indicePoint=i;
                chemin[*etape].distance=chemin[*etape-1].distance+euclidienne(listePoints[i].x,listePoints[i].y,listePoints[*lastClicked].x,listePoints[*lastClicked].y);
                *lastClicked = i;
            }

        }
    }
}

void afficherChemin(step_t* chemin, int etape)
{
    printf("CHEMIN ACTUEL : ");
    for(int i = 0; i<=etape; i++)
    {
        printf("%d ",chemin[i].indicePoint);
    }
    printf("DISTANCE ACTUELLE DU PARCOURS : %d\n", chemin[etape].distance);
    printf("\n");
}



void afficherTexteSDL(const char* texte, int x, int y, TTF_Font* police, SDL_Renderer* renderer)
{
    SDL_Surface* surfaceTexte = TTF_RenderText_Solid(police, texte, (SDL_Color) { .r = 255, .g = 255, .b = 255, .a = 255 });    
    SDL_Texture* textureTexte = SDL_CreateTextureFromSurface(renderer, surfaceTexte);
    
    int largeurTexte = surfaceTexte->w;
    int hauteurTexte = surfaceTexte->h;
    
    SDL_Rect rectDest;
    rectDest.x = x;
    rectDest.y = y;
    rectDest.w = largeurTexte;
    rectDest.h = hauteurTexte;
    
    SDL_RenderCopy(renderer, textureTexte, NULL, &rectDest);
    
    SDL_DestroyTexture(textureTexte);
    SDL_FreeSurface(surfaceTexte);
}



void gameLoop( SDL_Renderer * renderer, SDL_Window* window)
{
    SDL_bool program_on = SDL_TRUE;          
    SDL_Event event;
    

    step_t chemin[300];
    int etape = 0;

    chemin[0].distance=0;
    chemin[0].indicePoint=0;
                    
    int **matAdj = allouer(10);
    generer(matAdj,0,9);
    afficher(matAdj,10);
    printf("\n");
    genererGraphe(matAdj,10,0.2);
    afficher(matAdj,10);
    point_t listePoints[10];
    int lastClicked = 9;

    // listePoints[0].x=100;
    // listePoints[0].y=100;
    // listePoints[0].marque=0;
    
    // listePoints[1].x=500;
    // listePoints[1].y=200;
    // listePoints[1].marque=0;

    // listePoints[2].x=300;
    // listePoints[2].y=300;
    // listePoints[2].marque=0;

    // listePoints[3].x=600;
    // listePoints[3].y=400;
    // listePoints[3].marque=0;

    // listePoints[4].x=500;
    // listePoints[4].y=500;
    // listePoints[4].marque=0;

    // listePoints[5].x=100;
    // listePoints[5].y=300;
    // listePoints[5].marque=0;

    // listePoints[6].x=500;
    // listePoints[6].y=680;
    // listePoints[6].marque=0;

    // listePoints[7].x=1000;
    // listePoints[7].y=300;
    // listePoints[7].marque=0;

    // listePoints[8].x=600;
    // listePoints[8].y=200;
    // listePoints[8].marque=0;

    // listePoints[9].x=900;
    // listePoints[9].y=600;
    // listePoints[9].marque=1;
    
    uint8_t nbpoint=0;
    char nbpointTXT[25];
    sprintf(nbpointTXT,"Points restants : %d",nbpoint);
    TTF_Init();
    TTF_Font* police = TTF_OpenFont("RifficFree-Bold.ttf", 35);
    afficherTexteSDL("Mode editeur (cliquez pour ajouter un point)",0,0,police,renderer);
    SDL_RenderPresent(renderer); 
    while(nbpoint<10 && program_on == SDL_TRUE){
        if (SDL_PollEvent(&event) && event.type != SDL_MOUSEMOTION ) {
            switch(event.type) {
                case SDL_QUIT:
                    SDL_Quit();
                    program_on = SDL_FALSE;
                break;

                case SDL_MOUSEBUTTONDOWN:
                    if(event.button.button==SDL_BUTTON_LEFT)
                    {
                        
                        SDL_SetRenderDrawColor(renderer,0,0,0,255);      
                        SDL_RenderClear(renderer);
                        SDL_SetRenderDrawColor(renderer,255,255,255,255);    
                        listePoints[nbpoint].x=event.button.x;
                        listePoints[nbpoint].y=event.button.y;
                        if(nbpoint==0){
                            listePoints[nbpoint].marque=1;
                        } else {
                            listePoints[nbpoint].marque=0;
                        }
                        nbpoint+=1;
                        strcpy(nbpointTXT,"");
                        sprintf(nbpointTXT,"Points restants : %d",10-nbpoint);
                        afficherTexteSDL(nbpointTXT,0,0,police,renderer);
                        drawCircles(renderer,listePoints,nbpoint,lastClicked);
                        SDL_RenderPresent(renderer);                         
                    }
                break;
            }     
        }
    }
    lastClicked = 0;
    TTF_CloseFont(police);
    TTF_Quit();

    SDL_SetRenderDrawColor(renderer,0,0,0,255);      
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer,255,255,255,255);      
    drawGraph(renderer,matAdj,listePoints,10,lastClicked,chemin,etape);
    SDL_RenderPresent(renderer); 

    afficherChemin(chemin,etape);
    while (program_on == SDL_TRUE) {


        if (SDL_PollEvent(&event) && event.type != SDL_MOUSEMOTION ) {
            switch(event.type) {
                case SDL_QUIT:
                    
                    program_on = SDL_FALSE;
                break;

                case SDL_MOUSEBUTTONDOWN:
                    if(event.button.button==SDL_BUTTON_LEFT)
                    {
                        CircleClicked(event.button.x,event.button.y,listePoints,matAdj,10,&lastClicked,chemin,&etape);
                        SDL_SetRenderDrawColor(renderer,0,0,0,255);      
                        SDL_RenderClear(renderer);
                        SDL_SetRenderDrawColor(renderer,255,255,255,255);      
                        drawGraph(renderer,matAdj,listePoints,10,lastClicked,chemin,etape);
                        afficherChemin(chemin,etape);
                        SDL_RenderPresent(renderer); 

                    }
                break;
            }     
        }
    }
    
    SDL_Delay(100);


    liberer(matAdj,5);
// Affichages et calculs souvent ici

}
    
int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_DisplayMode screen; renderer = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    /*********************************************************************************************************************/  
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
        screen.w, screen.h);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("Premier dessin",
                SDL_WINDOWPOS_CENTERED,
                SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                screen.h * 0.66,
                SDL_WINDOW_OPENGL);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
    gameLoop(renderer,window);


    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;


}