#include "voyageur.h"
#include "structures.h"


#define RADIUS 20
#define PI 3.14159265359
///////////////////////////////////////////////////////////////////////////////////////////
//FONCTION UTILITAIRES
///////////////////////////////////////////////////////////////////////////////////////////
void end_sdl(char ok,char const* msg, fenetre_t SDL) {                         
    char msg_formated[255];                                            
    int l;                                                     
                                        
    if (!ok) {                                                       
        strncpy(msg_formated, msg, 250);                                         
        l = strlen(msg_formated);                                            
        strcpy(msg_formated + l, " : %s\n");                                                                             
        SDL_Log(msg_formated, SDL_GetError());                                   
    }                                                          
                                        
    if (SDL.renderer != NULL) {                                         
        SDL_DestroyRenderer(SDL.renderer);                                 
        SDL.renderer = NULL;
    }
    if (SDL.window != NULL)   {                                           
        SDL_DestroyWindow(SDL.window);                                    
        SDL.window= NULL;
    }
                                        
    SDL_Quit();                                                    
                                        
    if (!ok) {                                              
        exit(EXIT_FAILURE);                                                  
    }                                                          
}     



void drawPlanet(SDL_Texture* text, SDL_Renderer * renderer, coord_t pos)
{
    SDL_Rect 
          source = {0},                    // Rectangle définissant la zone totale de la planche
          window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
          state = {0};  



    SDL_QueryTexture(text,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);

    state.x=0;
    state.y=0;
    state.w=1280;
    state.h=1280;
    destination.x=pos.x-RADIUS*2-10;
    destination.y=pos.y-RADIUS*2-9;
    // float zoom = ((rand()%4)+6)/100;

    destination.h = 1280*0.08;
    destination.w = 1280*0.08;

    SDL_RenderCopy(renderer, text, &state,&destination); 
}

void drawCircles(SDL_Texture** tab, SDL_Renderer* renderer, point_t* listePoints, int nbpoints,int lastClicked)
{
    coord_t temp = init_coords(0,0);
    for(int i = 0; i <nbpoints; i++)
    {
        temp.x=listePoints[i].x; 
        temp.y=listePoints[i].y;
        if(listePoints[i].marque==0)
        {
            //filledCircleColor(renderer,tempx,tempy,RADIUS,0xFF000011);
            //SDL_SetTextureColorMod(tab[i],255,0,0);
        }
        else if(i==lastClicked)
        {
            //filledCircleColor(renderer,tempx,tempy,RADIUS,0xFF999911);
            //SDL_SetTextureColorMod(tab[i],0,255,0);

        }
        else {
            //filledCircleColor(renderer,tempx,tempy,RADIUS,0xFFFF0011);
            //SDL_SetTextureColorMod(tab[i],0,0,255);

        }
        drawPlanet(tab[i],renderer,temp);

    }


}



void drawGraph(SDL_Texture** tab, SDL_Renderer* renderer, int** matAdj, point_t* listePoints, int nbpoints,int lastClicked,step_t* chemin,int etape)
{

    

    drawCircles(tab,renderer,listePoints,nbpoints,lastClicked);
    SDL_SetRenderDrawColor(renderer,0,255,0,255);      
    int step = 0;
    for(int i = 0; i < nbpoints; i++)
    {
        for(int j = 0; j<nbpoints; j++)
        {
            if(matAdj[i][j]==1)
            {
                    SDL_SetRenderDrawColor(renderer,0,255,0,255);      
                    SDL_RenderDrawLine(renderer, listePoints[i].x, listePoints[i].y, listePoints[j].x, listePoints[j].y);
            }
        }
    }

    //ARCS PARCOURUS :
    while ((step+1)<=etape)
    {
        SDL_SetRenderDrawColor(renderer,255,0,255,255);      
        SDL_RenderDrawLine(renderer, listePoints[chemin[step].indicePoint].x,listePoints[chemin[step].indicePoint].y, listePoints[chemin[step+1].indicePoint].x, listePoints[chemin[step+1].indicePoint].y);
        step++;
    }
}

void drawBackground(SDL_Texture* text, fenetre_t SDL)
{
    //printf("OUI\n");
    SDL_Rect 
          source = {0},                    // Rectangle définissant la zone totale de la planche
          window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
          state = {0};  

    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
        &window_dimensions.w,
        &window_dimensions.h);

    SDL_QueryTexture(text,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);

    destination.x=0;
    destination.y=0;
    destination.h = window_dimensions.h;
    destination.w = window_dimensions.w;
    state.x=0;
    state.y=0;
    state.h=window_dimensions.h;
    state.w=window_dimensions.w;
    SDL_RenderCopy(SDL.renderer, text, &state,&destination);  
}

void drawStarz(SDL_Texture* text, fenetre_t SDL)
{
    SDL_Rect 
          source = {0},                    // Rectangle définissant la zone totale de la planche
          window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
          state = {0};  

    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
        &window_dimensions.w,
        &window_dimensions.h);

    SDL_QueryTexture(text,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);

    destination.x=0;
    destination.y=0;
    destination.h = window_dimensions.h;
    destination.w = window_dimensions.w;
    state.x=0;
    state.y=0;
    state.h=source.h;
    state.w=source.w;


    SDL_RenderCopy(SDL.renderer, text, &state,&destination);  
}

void drawProgressBar(fenetre_t SDL, int progressValue)
{
    int barX = 20;
    int barY = 20;

    int barWidth = 700;
    int barHeight = 50;

    SDL_Rect backgroundRect = { barX, barY, barWidth, barHeight };
    SDL_SetRenderDrawColor(SDL.renderer, 0,0,0,255);
    SDL_RenderFillRect(SDL.renderer, &backgroundRect);

    float progressWidth = (float)progressValue / 10 * barWidth;
    
    SDL_Rect progressRect = { barX, barY+5, progressWidth, 40 };

    SDL_SetRenderDrawColor(SDL.renderer,0,198,0,255);
    SDL_RenderFillRect(SDL.renderer, &progressRect);

}



void drawExplosion(SDL_Texture* text, int indice,coord_t pos, fenetre_t SDL)
{
    SDL_Rect 
          source = {0},                    // Rectangle définissant la zone totale de la planche
          window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
          state = {0};                     // Rectangle de la vignette en cours dans la planche 
    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
              &window_dimensions.w,
              &window_dimensions.h);
    SDL_QueryTexture(text,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);
    /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */
    float zoom = 0.3;                        // zoom, car ces images sont un peu petites
    int offset_x = 550,  // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = 550 ;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée
    state.w = offset_x;                    // Largeur de la vignette
    state.h = offset_y;   
    destination.x = pos.x;
    destination.y = pos.y;
    destination.w = offset_x * zoom;       // Largeur du sprite à l'écran
    destination.h = offset_y * zoom;       // Hauteur du sprite à l'écran
    state.x += indice*offset_x;                 // On passe à la vignette suivante dans l'image
    SDL_RenderCopy(SDL.renderer, text, &state,&destination);     
}


void afficher_vaisseau(SDL_Texture* texture, coord_t pos, float zoom, fenetre_t SDL) {
    SDL_Rect 
        source = {0},                    // Rectangle définissant la zone totale de la planche
        window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
        state = {0};  

    SDL_GetWindowSize(SDL.window,              // Récupération des dimensions de la fenêtre
        &window_dimensions.w,
        &window_dimensions.h);

    SDL_QueryTexture(texture,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);


    destination.h = window_dimensions.h*zoom;
    destination.w = window_dimensions.w*zoom;
    destination.x=pos.x-(destination.h/2);
    destination.y=pos.y-(destination.w/2);
    state.x=0;
    state.y=0;
    state.h=window_dimensions.h;
    state.w=window_dimensions.w;
    SDL_RenderCopy(SDL.renderer, texture, &state,&destination);  
}

//


void moveObject(SDL_Texture** planets,SDL_Texture * bg,SDL_Texture *stars, SDL_Texture * texture,SDL_Texture * textureEnnemi, coord_t start, coord_t end, int steps, int delay, int** matAdj, point_t* listePoints, 
int nbpoints,int lastClicked,step_t* chemin, int etape,  int id_adversaire, fenetre_t SDL) {
    float stepX = (float)(end.x-start.x) / steps; 
    float stepY = (float)(end.y-start.y) / steps;  
    

    coord_t current=init_coords(start.x,start.y);
    for (int i = 0; i <= steps; i++) {
        SDL_SetRenderDrawColor(SDL.renderer,0,0,0,255);      
        SDL_RenderClear(SDL.renderer);
        drawBackground(bg,SDL);
        drawStarz(stars,SDL);
        drawProgressBar(SDL,nbPointsParcouru(listePoints,nbpoints));
        SDL_SetRenderDrawColor(SDL.renderer,255,255,255,255); 
        drawGraph(planets,SDL.renderer,matAdj,listePoints,10,lastClicked,chemin,etape);
        afficher_vaisseau(texture,current,0.1,SDL);


        afficher_vaisseau(textureEnnemi,init_coords(listePoints[id_adversaire].x,listePoints[id_adversaire].y),0.1,SDL);
        
        SDL_RenderPresent(SDL.renderer); 
        
        current.x += stepX;
        current.y += stepY;
        
        SDL_Delay(delay);  
    }
}

void CircleClicked(coord_t mouse, point_t * listePoints, int** matAdj, int nbpoints,int* lastClicked,step_t *chemin, int* etape)
{
    

    for(int i = 0; i< nbpoints; i++)
    {
        if(euclidienne(mouse.x,mouse.y,listePoints[i].x,listePoints[i].y) < RADIUS)
        {
            if(matAdj[i][*lastClicked]==1 || i==*lastClicked)
            {
                if(listePoints[i].marque ==0) listePoints[i].marque=1;
                *etape=*etape+1;
                chemin[*etape].indicePoint=i;
                chemin[*etape].distance=chemin[*etape-1].distance+euclidienne(listePoints[i].x,listePoints[i].y,listePoints[*lastClicked].x,listePoints[*lastClicked].y);
                *lastClicked = i;
            }

        }
    }
}

void afficherChemin(step_t* chemin, int etape)
{
    printf("CHEMIN ACTUEL : ");
    for(int i = 0; i<=etape; i++)
    {
        printf("%d ",chemin[i].indicePoint);
    }
    printf("DISTANCE ACTUELLE DU PARCOURS : %d\n", chemin[etape].distance);
    printf("\n");
}



void afficherTexteSDL(const char* texte, coord_t pos, TTF_Font* police, SDL_Renderer* renderer)
{
    SDL_Surface* surfaceTexte = TTF_RenderText_Solid(police, texte, (SDL_Color) { .r = 255, .g = 255, .b = 255, .a = 255 });    
    SDL_Texture* textureTexte = SDL_CreateTextureFromSurface(renderer, surfaceTexte);
    
    int largeurTexte = surfaceTexte->w;
    int hauteurTexte = surfaceTexte->h;
    
    SDL_Rect rectDest;
    rectDest.x = pos.x;
    rectDest.y = pos.y;
    rectDest.w = largeurTexte;
    rectDest.h = hauteurTexte;
    
    SDL_RenderCopy(renderer, textureTexte, NULL, &rectDest);
    
    SDL_DestroyTexture(textureTexte);
    SDL_FreeSurface(surfaceTexte);
}


SDL_Texture* textureLoad(char * tex_name, fenetre_t SDL)
{
    SDL_Surface *my_image = NULL;          
    SDL_Texture* my_texture = NULL;         
    my_image = IMG_Load(tex_name);
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", SDL);
    my_texture = SDL_CreateTextureFromSurface(SDL.renderer, my_image);
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", SDL);
    SDL_FreeSurface(my_image);
    return my_texture;
}                                        



int deplacement_ennemi(int id_ennemi, int ** matAdj){
    int dest=id_ennemi;
    while(dest==id_ennemi|| matAdj[dest][id_ennemi]==0){
        dest=rand()%10;
    }
    return dest;
}



void gameLoop(fenetre_t SDL)
{
    SDL_bool program_on = SDL_TRUE;          
    SDL_Event event;

    SDL_Texture* bg =  textureLoad("tex/Nebula Aqua-Pink.png",SDL);
    SDL_Texture* joueur = textureLoad("joueur.png",SDL);
    SDL_Texture * stars =textureLoad("tex/Stars-Big_1_1_PC.png",SDL);
    SDL_Texture * explosion =textureLoad("tex/explosion.png",SDL);

    SDL_Texture* alien = textureLoad("alien.png",SDL);
    SDL_Texture** planets= malloc(10*sizeof(SDL_Texture*));


    planets[0] = textureLoad("tex/Planets/planet00.png",SDL);
    planets[1] = textureLoad("tex/Planets/planet01.png",SDL);
    planets[2] = textureLoad("tex/Planets/planet02.png",SDL);
    planets[3] = textureLoad("tex/Planets/planet03.png",SDL);
    planets[4] = textureLoad("tex/Planets/planet04.png",SDL);
    planets[5] = textureLoad("tex/Planets/planet05.png",SDL);
    planets[6] = textureLoad("tex/Planets/planet06.png",SDL);
    planets[7] = textureLoad("tex/Planets/planet07.png",SDL);
    planets[8] = textureLoad("tex/Planets/planet08.png",SDL);
    planets[9] = textureLoad("tex/Planets/planet09.png",SDL);




    step_t chemin[300];
    point_t listePoints[10];

    int etape = 0;
    int **matAdj = allouer(10);
    int lastClicked = 9;

    chemin[0].distance=0;
    chemin[0].indicePoint=0;
    



    generer(matAdj,0,9);
    afficher(matAdj,10);
    printf("\n");
    generer(matAdj,0,2);
    genererGraphe(matAdj,10,0.01);
    afficher(matAdj,10);


    uint8_t nbpoint=0;
    char nbpointTXT[32];
    sprintf(nbpointTXT,"Points restants : %d",nbpoint);
    TTF_Init();
    TTF_Font* police = TTF_OpenFont("RifficFree-Bold.ttf", 35);
    drawBackground(bg,SDL);
    drawStarz(stars,SDL);
    // drawProgressBar(SDL,nbPointsParcouru(listePoints,nbpoint));

    afficherTexteSDL("Mode editeur (cliquez pour ajouter une planete)",init_coords(0,0),police,SDL.renderer);

    SDL_RenderPresent(SDL.renderer); 
    while(nbpoint<10 && program_on == SDL_TRUE){
        if (SDL_PollEvent(&event) && event.type != SDL_MOUSEMOTION ) {
            switch(event.type) {
                case SDL_QUIT:
                    SDL_Quit();
                    program_on = SDL_FALSE;
                break;

                case SDL_MOUSEBUTTONDOWN:
                    if(event.button.button==SDL_BUTTON_LEFT)
                    {
                        SDL_SetRenderDrawColor(SDL.renderer,0,0,0,255);      
                        SDL_RenderClear(SDL.renderer);
                        SDL_SetRenderDrawColor(SDL.renderer,255,255,255,255);    
                        listePoints[nbpoint].x=event.button.x;
                        listePoints[nbpoint].y=event.button.y;
                        if(nbpoint==0){
                            listePoints[nbpoint].marque=1;
                        } else {
                            listePoints[nbpoint].marque=0;
                        }
                        nbpoint+=1;
                        strcpy(nbpointTXT,"");
                        sprintf(nbpointTXT,"Planetes restantes : %d",10-nbpoint);
                        drawBackground(bg,SDL);
                        drawStarz(stars,SDL);
                        // drawProgressBar(SDL,nbPointsParcouru(listePoints,nbpoint));
                        afficherTexteSDL(nbpointTXT,init_coords(0,0),police,SDL.renderer);
                        drawCircles(planets,SDL.renderer,listePoints,nbpoint,lastClicked);
                        SDL_RenderPresent(SDL.renderer);                         
                    }
                break;
            }     
        }
    }
    lastClicked = 0;
    TTF_CloseFont(police);
    TTF_Quit();
    



    

    int id_ennemi=(rand()%9)+1;
    





    

    //afficherChemin(chemin,etape);
    

    coord_t pos_anc,pos_anc_ennemi;

    int coef_levit=0;
    

    while (program_on == SDL_TRUE) {
        SDL_SetRenderDrawColor(SDL.renderer,0,0,0,255);      
        SDL_RenderClear(SDL.renderer);
        drawBackground(bg,SDL);
        drawStarz(stars,SDL);
        drawProgressBar(SDL,nbPointsParcouru(listePoints,nbpoint));
        SDL_SetRenderDrawColor(SDL.renderer,255,255,255,255);      
        drawGraph(planets,SDL.renderer,matAdj,listePoints,10,lastClicked,chemin,etape);

        afficher_vaisseau(joueur,init_coords(listePoints[lastClicked].x,listePoints[lastClicked].y+10*sin(((coef_levit*2*PI)/100))),0.1,SDL);
        afficher_vaisseau(alien,init_coords(listePoints[id_ennemi].x,listePoints[id_ennemi].y+10*sin(((coef_levit*2*PI)/100))),0.1,SDL);

        SDL_RenderPresent(SDL.renderer); 

        
        if (SDL_PollEvent(&event) && event.type != SDL_MOUSEMOTION ) {
            switch(event.type) {
                case SDL_QUIT:
                    
                    program_on = SDL_FALSE;
                break;

                case SDL_MOUSEBUTTONDOWN:
                    if(event.button.button==SDL_BUTTON_LEFT)
                    {
                        pos_anc=init_coords(listePoints[lastClicked].x,listePoints[lastClicked].y);              
                        

                        CircleClicked(init_coords(event.button.x,event.button.y),listePoints,matAdj,10,&lastClicked,chemin,&etape);
                        
                        if(pos_anc.x!=listePoints[lastClicked].x || pos_anc.y!=listePoints[lastClicked].y){
                            
                            moveObject(planets,bg,stars,joueur,alien,pos_anc,init_coords(listePoints[lastClicked].x,listePoints[lastClicked].y),20,50,
                            matAdj,listePoints,10,lastClicked,chemin,etape, id_ennemi, SDL);

                            if(id_ennemi==lastClicked){
                                printf("PERDU !!!!\n");
                                for(int i = 0; i < 10; i++)
                                {
                                    
                                    SDL_RenderClear(SDL.renderer);
                                    drawBackground(bg,SDL);
                                    drawStarz(stars,SDL);
                                    drawProgressBar(SDL,nbPointsParcouru(listePoints,nbpoint));
                                    SDL_SetRenderDrawColor(SDL.renderer,255,255,255,255);      
                                    drawGraph(planets,SDL.renderer,matAdj,listePoints,10,lastClicked,chemin,etape);
                                    drawExplosion(explosion,i,init_coords(listePoints[lastClicked].x-RADIUS*5,listePoints[lastClicked].y-RADIUS*5),SDL);
                                    SDL_RenderPresent(SDL.renderer);                         
                                    SDL_Delay(50);
                                }
                                program_on=SDL_FALSE;
                            } else {
                                pos_anc_ennemi.x=listePoints[id_ennemi].x;
                                pos_anc_ennemi.y=listePoints[id_ennemi].y;
                                id_ennemi =  deplacement_ennemi(id_ennemi,matAdj);
                                moveObject(planets,bg,stars,alien,joueur, pos_anc_ennemi,init_coords(listePoints[id_ennemi].x,listePoints[id_ennemi].y),20,50,
                                matAdj,listePoints,10,lastClicked,chemin,etape, lastClicked, SDL);

                                pos_anc_ennemi=init_coords(listePoints[id_ennemi].x,listePoints[id_ennemi].y);
                            }
                            if(grapheParcouru(listePoints,10)){
                                printf("GAGNE !!!\n");
                                program_on=SDL_FALSE;
                            }
                            
                        }
                        if(id_ennemi==lastClicked){
                            printf("PERDU !!\n");
                              for(int i = 0; i < 10; i++)
                                {
                                    SDL_RenderClear(SDL.renderer);
                                    drawBackground(bg,SDL);
                                    drawStarz(stars,SDL);
                                    drawProgressBar(SDL,nbPointsParcouru(listePoints,nbpoint));
                                    SDL_SetRenderDrawColor(SDL.renderer,255,255,255,255);      
                                    drawGraph(planets,SDL.renderer,matAdj,listePoints,10,lastClicked,chemin,etape);
                                    drawExplosion(explosion,i,init_coords(listePoints[lastClicked].x-RADIUS*5,listePoints[lastClicked].y-RADIUS*5), SDL);
                                    SDL_RenderPresent(SDL.renderer);                         
                                    SDL_Delay(50);

                                }
                            program_on=SDL_FALSE;
                        }
                    }
                break;
            }     
        }
        coef_levit=(coef_levit+1)%100;
        SDL_Delay(10);
    }

    
    


    liberer(matAdj,5);
// Affichages et calculs souvent ici

}




    
int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    fenetre_t SDL;
    SDL.renderer=NULL;
    SDL.window=NULL;
    
    SDL_DisplayMode screen; SDL.renderer = SDL_CreateRenderer(SDL.window, -1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    /*********************************************************************************************************************/  
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", SDL);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
        screen.w, screen.h);

    /* Création de la fenêtre */
    SDL.window = SDL_CreateWindow("Premier dessin",
                SDL_WINDOWPOS_CENTERED,
                SDL_WINDOWPOS_CENTERED, screen.w*0.9,
                screen.h*0.9,
                SDL_WINDOW_OPENGL);
    if (SDL.window == NULL) end_sdl(0, "ERROR WINDOW CREATION", SDL);

    /* Création du renderer */
    SDL.renderer = SDL_CreateRenderer(SDL.window, -1,
                    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (SDL.renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", SDL);
    gameLoop(SDL);


    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", SDL);
    return EXIT_SUCCESS;
}